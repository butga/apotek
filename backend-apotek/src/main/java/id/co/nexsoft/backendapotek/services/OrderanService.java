package id.co.nexsoft.backendapotek.services;

import id.co.nexsoft.backendapotek.dto.request.OrderanDetailRequest;
import id.co.nexsoft.backendapotek.dto.request.OrderanRequest;
import id.co.nexsoft.backendapotek.dto.response.HTTPResponse;
import id.co.nexsoft.backendapotek.dto.response.HTTPResponses;
import id.co.nexsoft.backendapotek.dto.response.MyOrderResponse;
import id.co.nexsoft.backendapotek.entities.Orderan;
import id.co.nexsoft.backendapotek.entities.User;
import id.co.nexsoft.backendapotek.repositories.ObatRepository;
import id.co.nexsoft.backendapotek.repositories.OrderanDetailRepository;
import id.co.nexsoft.backendapotek.repositories.OrderanRepository;
import id.co.nexsoft.backendapotek.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderanService {

    @Autowired
    private OrderanRepository orderanRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObatRepository obatRepository;

    @Autowired
    private OrderanDetailRepository orderanDetailRepository;

    @Transactional
    public ResponseEntity<?> checkoutOrderan(OrderanRequest or) {

        // cek login
        User userRepo = userRepository.findUser(or.getUserId());
        if(!userRepo.isLogin())
            return ResponseEntity.badRequest()
                    .body(new HTTPResponse(false, "Silahkan login terlebih dahulu", null));

        // cek hak akses
        if(!userRepo.getRole().equals("buyer"))
            return ResponseEntity.badRequest()
                    .body(new HTTPResponse(false, "forbidden!", null));

        // cek stok dulu
        for( Integer idObat : or.getListObat()) {
            int stock = obatRepository.checkStock(idObat);
            if( stock == 0) {
                String obatName = obatRepository.getObatName(idObat);
                return ResponseEntity.badRequest()
                        .body(new HTTPResponse(false, "Stok "+obatName+" tidak mencukupi", null));
            }
        }

        // buat orderan
        or.setTanggalOrder(LocalDate.now());
        or.setStatus("unpaid");
        orderanRepository.checkoutOrderan(or);
        Orderan orderan = orderanRepository.findTopByOrderByIdDesc();

        // buat order detail
        for( Integer idObat : or.getListObat()) {
            OrderanDetailRequest odr = new OrderanDetailRequest();
            odr.setObatId(idObat);
            odr.setOrderanId(orderan.getId());
            obatRepository.updateStokObat(idObat);
            orderanDetailRepository.checkoutOrderanDetail(odr);
        }

        return ResponseEntity.ok()
                .body(new HTTPResponse(true, "sukses checkout orderan", orderan));
    }


    public ResponseEntity<?> findMyOrderans(int id) {

        List<MyOrderResponse> listOrderan = new ArrayList();

        for( MyOrderResponse orderan : orderanRepository.findMyOrderans(id)) {
            orderan.setJumlahOrder(orderanDetailRepository.getJumlahOrder(orderan.getId()));
            listOrderan.add(orderan);
        }

        return ResponseEntity.ok()
                .body(new HTTPResponses(true, null, listOrderan));
    }

    @Transactional
    public ResponseEntity<?> confirmOrderan(int id) {

        orderanRepository.confirmOrderan(id, "paid");

        Orderan orderanUpdated = orderanRepository.findById(id);

        return ResponseEntity.ok()
                .body(new HTTPResponse(true, "Berhasil Confirm Orderan", orderanUpdated));
    }

    @Transactional
    public ResponseEntity<?> rejectOrderan(int id) {

        orderanRepository.rejectOrderan(id);

        return ResponseEntity.ok()
                .body(new HTTPResponse(true, "Berhasil Reject Orderan", null));
    }

    public ResponseEntity<?> findOrderanRequest() {

        List<MyOrderResponse> listOrderan = new ArrayList();

        for( MyOrderResponse orderan : orderanRepository.findOrderanRequest("unpaid")) {
            orderan.setJumlahOrder(orderanDetailRepository.getJumlahOrder(orderan.getId()));
            listOrderan.add(orderan);
        }

        return ResponseEntity.ok()
                .body(new HTTPResponse(true, null, listOrderan));
    }

    public ResponseEntity<?> findOrderanDone() {

        List<MyOrderResponse> listOrderan = new ArrayList();

        for( MyOrderResponse orderan : orderanRepository.findOrderanRequest("paid")) {
            orderan.setJumlahOrder(orderanDetailRepository.getJumlahOrder(orderan.getId()));
            listOrderan.add(orderan);
        }

        return ResponseEntity.ok()
                .body(new HTTPResponse(true, null, listOrderan));
    }
}
