package id.co.nexsoft.backendapotek.controllers;

import id.co.nexsoft.backendapotek.dto.request.OrderanRequest;
import id.co.nexsoft.backendapotek.services.OrderanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class OrderanController {

    @Autowired
    private OrderanService orderanService;

    @PostMapping("/api/buyers/checkout")
    public ResponseEntity<?> checkoutOrderan(@Valid @RequestBody OrderanRequest or) {
        return orderanService.checkoutOrderan(or);
    }

    @GetMapping("/api/buyers/orderans/{id}")
    public ResponseEntity<?> findMyOrderans(@PathVariable int id) {
        return orderanService.findMyOrderans(id);
    }

    @GetMapping("/api/buyers/orderans/confirm")
    public ResponseEntity<?> findOrderanRequest() {
        return orderanService.findOrderanRequest();
    }

    @GetMapping("/api/buyers/orderans")
    public ResponseEntity<?> findOrderanDone() {
        return orderanService.findOrderanDone();
    }

    @PostMapping("/api/admin/orderans/confirm/{id}")
    public ResponseEntity<?> confirmOrderan(@PathVariable int id) {
        return orderanService.confirmOrderan(id);
    }

    @PostMapping("/api/admin/orderans/reject/{id}")
    public ResponseEntity<?> rejectOrderan(@PathVariable int id) {
        return orderanService.rejectOrderan(id);
    }

}
