package id.co.nexsoft.backendapotek.controllers;

import id.co.nexsoft.backendapotek.dto.request.LoginRequest;
import id.co.nexsoft.backendapotek.entities.User;
import id.co.nexsoft.backendapotek.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/api/users/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody User user) {
        return userService.registerBuyer(user);
    }

    @PostMapping("/api/admins/register")
    public ResponseEntity<?> registerAdmin(@Valid @RequestBody User user) {
        return userService.registerAdmin(user);
    }

    @PostMapping("/api/users/login")
    public ResponseEntity<?> loginUser(@Valid @RequestBody LoginRequest loginRequest) {
        return userService.loginUser(loginRequest);
    }

    @PostMapping("/api/users/logout/{id}")
    public ResponseEntity<?> loginUser(@PathVariable int id) {
        return userService.logoutUser(id);
    }

    @GetMapping("/api/admins/users")
    public ResponseEntity<?> findAllUser() {
        return userService.findAllUser();
    }

}
