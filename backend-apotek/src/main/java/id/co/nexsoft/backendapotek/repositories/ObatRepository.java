package id.co.nexsoft.backendapotek.repositories;

import id.co.nexsoft.backendapotek.entities.Obat;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ObatRepository extends CrudRepository<Obat, Integer> {

    @Modifying
    @Query(value = "INSERT INTO obat(id, nama_obat, stok_obat, jenis_obat, harga_obat, dosis, data_image) " +
            "VALUES(:#{#o.id}, :#{#o.namaObat}, :#{#o.stokObat}, :#{#o.jenisObat}, :#{#o.hargaObat}, :#{#o.dosis}, :#{#o.dataImage})"
            , nativeQuery = true)
    void saveObat(@Param("o") Obat obat);

    @Modifying
    @Query(value = "UPDATE obat o " +
            "SET o.dosis = :#{#o.dosis}, o.nama_obat = :#{#o.namaObat}, o.stok_obat = :#{#o.stokObat}, o.jenis_obat = :#{#o.jenisObat}, o.harga_obat = :#{#o.hargaObat} " +
            "WHERE o.id = :id"
            , nativeQuery = true)
    void editObat(@Param("o") Obat obat, @Param("id") int id);

    @Query(value ="SELECT * FROM obat o " +
            "WHERE o.tgl_dihapus IS NULL", nativeQuery = true)
    List<Obat> findAllObat();

    @Modifying
    @Query(value = "UPDATE obat o " +
            "SET o.tgl_dihapus = CURDATE() " +
            "WHERE o.id = :id"
            , nativeQuery = true)
    void deleteObat(@Param("id") int id);

    @Query( value="SELECT nama_obat FROM obat o WHERE o.id = :idObat ", nativeQuery = true)
    String getObatName(@Param("idObat") int id);

    @Query( value = "SELECT EXISTS (SELECT id FROM obat o WHERE o.stok_obat >= 1 AND o.id = :idObat)", nativeQuery = true)
    int checkStock(@Param("idObat") int id);

    @Modifying
    @Query( value = "UPDATE obat o SET o.stok_obat = (o.stok_obat - 1) WHERE o.id = :idObat", nativeQuery = true)
    void updateStokObat(@Param("idObat") int id);

}
