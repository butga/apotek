package id.co.nexsoft.backendapotek.controllers;

import id.co.nexsoft.backendapotek.entities.Obat;
import id.co.nexsoft.backendapotek.services.ObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ObatController {

    @Autowired
    private ObatService obatService;

    @GetMapping("/api/admin/obats")
    public ResponseEntity<?> findAllObat() {
        return obatService.findAllObat();
    }

    @PostMapping("/api/admin/obats")
    public ResponseEntity<?> addObat(@Valid @RequestBody Obat obat) {
        return obatService.addObat(obat);
    }

    @PostMapping("/api/admin/obats/{id}")
    public ResponseEntity<?> editObat(@Valid @RequestBody Obat obat, @PathVariable int id) {
        return obatService.editObat(obat, id);
    }

    @PostMapping("/api/admin/obats/delete/{id}")
    public ResponseEntity<?> deleteObat(@PathVariable int id) {
        return obatService.deleteObat(id);
    }
}
