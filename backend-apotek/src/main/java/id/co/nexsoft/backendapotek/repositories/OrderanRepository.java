package id.co.nexsoft.backendapotek.repositories;

import id.co.nexsoft.backendapotek.dto.request.OrderanRequest;
import id.co.nexsoft.backendapotek.dto.response.MyOrderResponse;
import id.co.nexsoft.backendapotek.entities.Orderan;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderanRepository extends CrudRepository<Orderan, Integer> {

    @Modifying
    @Query( value = "INSERT INTO orderan(id, status, tanggal_order, user_id) " +
            "VALUES(:#{#order.id}, :#{#order.status}, :#{#order.tanggalOrder}, :#{#order.userId})", nativeQuery = true)
    void checkoutOrderan(@Param("order") OrderanRequest or);

    Orderan findTopByOrderByIdDesc();

    @Query( value = "SELECT new id.co.nexsoft.backendapotek.dto.response.MyOrderResponse" +
            "(o.id, o.tanggalOrder, o.status) " +
            "FROM Orderan o " +
            "WHERE o.user.id = :id " +
            "ORDER BY o.id DESC")
    List<MyOrderResponse> findMyOrderans(@Param("id") int id);

    @Query( value = "SELECT new id.co.nexsoft.backendapotek.dto.response.MyOrderResponse" +
            "(o.id, o.tanggalOrder, o.status) " +
            "FROM Orderan o " +
            "WHERE o.status = :status " +
            "ORDER BY o.id DESC")
    List<MyOrderResponse> findOrderanRequest(@Param("status") String status);

    Orderan findById(int id);

    @Modifying
    @Query( value = "UPDATE orderan o SET o.status = :status WHERE o.id = :id", nativeQuery = true)
    void confirmOrderan(@Param("id") int idOrderan, @Param("status") String status);

    @Modifying
    @Query( value="DELETE FROM Orderan WHERE id = :id")
    void rejectOrderan(@Param("id") int id);

    @Query( value = "SELECT COUNT(id) FROM orderan o " +
            "WHERE o.status = :status AND o.tanggal_order = CURDATE()", nativeQuery = true )
    int orderToday(@Param("status") String status);

    @Query( value = "SELECT sum(ob.harga_obat) " +
            "FROM orderan o " +
            "JOIN orderan_detail od " +
            "ON o.id = od.orderan_id " +
            "JOIN obat ob " +
            "ON ob.id = od.obat_id " +
            "WHERE o.status = :status AND o.tanggal_order = CURDATE()", nativeQuery = true )
    long pendapatan(@Param("status") String status);

}
