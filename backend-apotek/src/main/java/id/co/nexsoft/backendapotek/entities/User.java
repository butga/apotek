package id.co.nexsoft.backendapotek.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "user",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "no_telp"),
                @UniqueConstraint(columnNames = "email")
        })
public class User {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private int id;

    @NotNull
    @NotBlank
    @NotEmpty
    private String namaLengkap;

    @NotNull
    @NotBlank
    @NotEmpty
    @Size( max = 20)
    private String username;

    @NotNull
    @NotBlank
    @NotEmpty
    private String password;

    @NotNull
    @NotBlank
    @NotEmpty
    private String alamat;

    @NotNull
    @NotBlank
    @NotEmpty
    private String no_telp;

    @NotNull
    @NotBlank
    @NotEmpty
    private String email;

    private LocalDate tgl_registrasi;

    private String role;

    @Column(columnDefinition = "boolean default false")
    private boolean isLogin;

}
