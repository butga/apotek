package id.co.nexsoft.backendapotek.services;

import id.co.nexsoft.backendapotek.dto.response.HTTPResponse;
import id.co.nexsoft.backendapotek.dto.response.HTTPResponses;
import id.co.nexsoft.backendapotek.entities.Obat;
import id.co.nexsoft.backendapotek.entities.User;
import id.co.nexsoft.backendapotek.repositories.ObatRepository;
import id.co.nexsoft.backendapotek.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ObatService {

    @Autowired
    private ObatRepository obatRepository;

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public ResponseEntity<?> addObat(Obat obat) {

        obatRepository.saveObat(obat);
        return ResponseEntity.ok()
                .body(new HTTPResponse(true, "Data berhasil ditambahkan", obat));
    }

    public ResponseEntity<?> findAllObat() {
        List<Obat> listObat = obatRepository.findAllObat();
        return ResponseEntity.ok()
                .body(new HTTPResponses(true, null, listObat));
    }

    @Transactional
    public ResponseEntity<?> editObat(Obat obat, int id) {

        obatRepository.editObat(obat, id);

        List<Obat> listObat = obatRepository.findAllObat();
        return ResponseEntity.ok()
                .body(new HTTPResponses(true, "Berhasil edit obat", listObat));
    }

    @Transactional
    public ResponseEntity<?> deleteObat(int id) {

        obatRepository.deleteObat(id);

        List<Obat> listObat = obatRepository.findAllObat();
        return ResponseEntity.ok()
                .body(new HTTPResponses(true, "Berhasil hapus obat", listObat));
    }
}
