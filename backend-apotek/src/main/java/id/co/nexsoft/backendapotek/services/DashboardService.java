package id.co.nexsoft.backendapotek.services;

import id.co.nexsoft.backendapotek.dto.response.DashboardResponse;
import id.co.nexsoft.backendapotek.dto.response.HTTPResponse;
import id.co.nexsoft.backendapotek.repositories.OrderanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class DashboardService {

    @Autowired
    private OrderanRepository orderanRepository;

    public ResponseEntity<?> findDataDashboard() {

        DashboardResponse dr = new DashboardResponse(0,0);

        // cek apakah ada orderan hari ini
        if(orderanRepository.orderToday("paid") >= 1) {
            dr.setJumlahOrder(orderanRepository.orderToday("paid"));
            dr.setPendapatan(orderanRepository.pendapatan("paid"));
        }

        return ResponseEntity.ok()
                .body(new HTTPResponse(true, null, dr));
    }
}
