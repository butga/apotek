package id.co.nexsoft.backendapotek.controllers;

import id.co.nexsoft.backendapotek.services.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    @GetMapping("/api/admins/dashboard")
    public ResponseEntity<?> findDataDashboard() {
        return dashboardService.findDataDashboard();
    }

}
