package id.co.nexsoft.backendapotek.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class LoginResponse {

    private int id;

    private String nama;

    private String username;

    private String alamat;

    private String no_telp;

    private String email;

    private String role;

    private boolean isLogin;

}
