import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router';
import DashboardAdmin from './pages/admin/DashboardAdmin';
import BuyerPage from './pages/buyer/BuyerPage';
import LoginPage from './pages/LoginPage'
import RegisterPage from './pages/RegisterPage';

const RouterPage = () => {
    const login = useSelector((state) => state.auth.user.login);
    const role = useSelector((state) => state.auth.user.role);

    const AdminRoute = ({...props}) => {
        return (
            (login !== false && role === 'admin') ? <Route {...props} /> : <Redirect to="/login" />
        )
    }

    const BuyerRoute = ({...props}) => {
        return (
            (login !== false && role === 'buyer') ? <Route {...props} /> : <Redirect to="/login" />
        )
    }

    const CostumRoute = ({...props}) => {
        return (
            login ? <Redirect to={role === "admin" ? "/admin" : "/buyer"} /> : <Route {...props} />
        )
    }

    return (
        <Switch>
            <Route exact path="/"><Redirect to="/buyer" /></Route>
            <CostumRoute exact path="/login" component={LoginPage} />
            <Route exact path="/register" component={RegisterPage} />
            <BuyerRoute path="/buyer" component={BuyerPage} />
            <AdminRoute path="/admin" component={DashboardAdmin} />
        </Switch>
    );
}

export default RouterPage;