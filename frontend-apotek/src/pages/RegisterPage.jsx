import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import '../assets/css/registerPage.css';
import { useHistory } from 'react-router';
import { postData } from '../service/Fetch';
import swal from 'sweetalert';


const RegisterPage = () => {

    let history = useHistory();

    const [data, setData] = useState({
        namaLengkap: '',
        username: '',
        password: '',
        alamat: '',
        no_telp: '',
        email: ''
    });

    const registerHandler = (e) => {

        e.preventDefault();

        postData("users/register", data)
            .then(res => {
                swal("Error", res.data.message, "error");
                history.push("/login")
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            })
    }

    console.log(data);

    return (
        <div className="body-background">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8 col-lg-6 mt-2">
                        <div className="card-custom mt-5 trans">
                            <div className="card-header bg-transparent mb-0 text-center">
                                <h1 className="pt-3">NEXCARES</h1>
                                <p className="mt-2">Register to create your account</p>
                            </div>
                            <div className="card-body">
                                <form
                                    onSubmit={registerHandler}
                                >
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor="name" className="mb-2">Full Name</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, namaLengkap: e.target.value })}
                                                    required
                                                    placeholder="Full Name" />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor="username" className="mb-2">Username</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, username: e.target.value })}
                                                    required
                                                    placeholder="Username" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group mt-3">
                                                <label htmlFor="email" className="mb-2">Email</label>
                                                <input
                                                    type="email"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, email: e.target.value })}
                                                    required
                                                    placeholder="Email" />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group mt-3">
                                                <label htmlFor="phone" className="mb-2">Phone Number</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, no_telp: e.target.value })}
                                                    required
                                                    pattern="^(?=.\d.).{9,12}$"
                                                    title="Must be number 9-12 Digit"
                                                    placeholder="Phone Number" />
                                                <div class="invalid-tooltip">
                                                    Must be number 9-12 Digit
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group mt-3 mb-4">
                                                <label htmlFor="password" className="mb-2">Password</label>
                                                <input
                                                    type="password"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, password: e.target.value })}
                                                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$"
                                                    title="Minimum 6 Character, Uppercase, Lowercase, Number"
                                                    required
                                                    placeholder="Password" />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group mt-3 mb-4">
                                                <label htmlFor="email" className="mb-2">Address</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    onChange={(e) => setData({ ...data, alamat: e.target.value })}
                                                    required
                                                    placeholder="Address" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group d-flex justify-content-end">
                                        <input type="submit" value="Register" className="btn btn-register" />
                                    </div>
                                    <div>
                                        <p className="mt-2">Already have an account? <Link to="/login">Log in</Link></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default RegisterPage;