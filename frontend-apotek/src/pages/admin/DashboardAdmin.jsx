import React from 'react';
import ContentAdmin from '../../components/admin/content/ContentAdmin';
import NavbarAdmin from '../../components/admin/NavbarAdmin';

const DashboardAdmin = () => {
    return (
        <>
            <NavbarAdmin />
            <ContentAdmin />
        </>
    );
}

export default DashboardAdmin;