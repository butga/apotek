import React from 'react';
import ContentBuyer from '../../components/buyer/content/ContentBuyer';
import NavbarBuyer from '../../components/buyer/NavbarBuyer';

const BuyerPage = () => {
    return (
        <>
            <NavbarBuyer />
            <ContentBuyer />
        </>
    );
}
 
export default BuyerPage;