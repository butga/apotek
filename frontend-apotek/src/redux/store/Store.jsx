import { configureStore } from "@reduxjs/toolkit";
import AuthReducer from "../reducer/AuthReducer";
import { combineReducers } from "redux";
import storage from "redux-persist/lib/storage";
import {
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from "redux-persist";
import KeranjangReducer from "../reducer/KeranjangReducer";

const rootReducer = combineReducers({
    auth: AuthReducer,
    keranjang :KeranjangReducer
});

const persistConfig = {
    key: "root",
    version: 1,
    storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) => 
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            },
        }),
});

export default store;