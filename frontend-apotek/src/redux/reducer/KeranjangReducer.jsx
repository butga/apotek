import { createSlice } from "@reduxjs/toolkit";

const initialKeranjangState = {
    userId: '',
    listObat: [],
    total: 0
}

const keranjangSlice = createSlice({
    name: "auth",
    initialState: initialKeranjangState,
    reducers: {
        tambah(state, action) {
            state.listObat = [...state.listObat, action.payload];
            state.total++;
        },
        remove(state, action) {
            state.listObat.splice(action.payload, 1);
            state.total = state.total - 1;
        },
        removeAll(state) {
            state.userId = ''
            state.listObat = []
            state.total = 0
        }
    }
})

export const keranjangAction = keranjangSlice.actions;

export default keranjangSlice.reducer;