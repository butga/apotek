const LoadingComp = () => {
    return (
        <div className="container">
            <div className="row">
                <div className="d-flex justify-content-center mb-5">
                    <div className="text-center mb-5">
                        <div className="spinner-border mb-3 mt-5" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                        <div className="mb-5">
                            <h5>Loading...</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default LoadingComp;