import React from 'react'
import { Link } from 'react-router-dom';
import { Col, Row } from 'reactstrap';

const BreadcrumbComp = (props) => {
    return (
        <Row className="mt-4 mb-4">
            <Col>
                <h5>{props.title}</h5>
            </Col>
            <Col className="flex-column align-items-end d-none d-md-flex">
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to={props.link}><i class="bi bi-house"></i></Link></li>
                        <li className="breadcrumb-item active" aria-current="page">{props.breadcrumb}</li>
                    </ol>
                </nav>
            </Col>
        </Row>
    );
}

export default BreadcrumbComp;