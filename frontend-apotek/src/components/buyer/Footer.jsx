import React from 'react';
import '../../assets/css/footer.css'

const Footer = () => {
    return (
        <footer className="footer">
            <p className="text-center">© 2021 NEXCARES</p>
        </footer>
    );
}

export default Footer;