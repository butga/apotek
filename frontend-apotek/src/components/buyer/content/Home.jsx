import React, { useEffect, useState } from 'react'
import HeroBanner from './HeroBanner';
import '../../../assets/css/homeBuyer.css'
import { Col, Container, Row } from 'reactstrap';
import Product from '../card/Product';
import { getData } from '../../../service/Fetch';
import swal from 'sweetalert';
import Footer from '../Footer';

const Home = () => {

    const [datas, setDatas] = useState([]);

    useEffect(() => {
        const url = `admin/obats`;
        getData(url)
            .then(res => {
                setDatas(res.data.data);
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            })
    }, [])

    console.log(datas);

    return (
        <>
            <HeroBanner />
            <Container>
                <Row>
                    <h5
                        className="mt-4 mb-3 text-center">
                        Mitra Kami
                    </h5>
                </Row>
                <Row className="d-flex justify-content-center mt-3 mb-4">
                    <Col sm="2">
                        <img
                            className="logo-mitra"
                            src="images/logo1.png" alt="" />
                    </Col>
                    <Col sm="2">
                        <img
                            className="logo-mitra"
                            src="images/logo2.png" alt="" />
                    </Col>
                    <Col sm="2">
                        <img
                            className="logo-mitra"
                            src="images/logo3.png" alt="" />
                    </Col>
                </Row>
                <Row>
                    <h5
                        className="mt-4 mb-4">
                        Produk
                    </h5>
                </Row>
                <Row
                    className="mb-3">
                    {
                        datas.map((data) =>
                            <Col sm="3">
                                <Product
                                    product={data}
                                />
                            </Col>
                        )
                    }
                </Row>
            </Container>
            <Footer/>
        </>
    );
}
export default Home;