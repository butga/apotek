import React, { useEffect, useState } from 'react'
import CurrencyFormat from 'react-currency-format';
import { keranjangAction } from '../../../redux/reducer/KeranjangReducer';
import { Container, Row, Col } from 'reactstrap';
import '../../../assets/css/keranjang.css'
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import swal from 'sweetalert';
import Footer from '../Footer';
import { getData, postData } from '../../../service/Fetch';

const Keranjang = () => {

    const keranjangs = useSelector((state) => state.keranjang.listObat);

    const [subTotal, setSubTotal] = useState(0);

    const action = useSelector((state) => state.keranjang.total);

    const totalKeranjang = useSelector((state) => state.keranjang.total);

    const id = useSelector((state) => state.auth.user.id);

    const dispatch = useDispatch();

    useEffect(() => {
        let total = 0;
        keranjangs.forEach(data => {
            total = total + data.hargaObat;
        });
        setSubTotal(total)
    }, [action])

    const removeHandler = (index) => {
        swal({
            title: "Hapus obat?",
            text: "Apakah anda yakin untuk menghapus obat ini dari keranjang?",
            icon: "info",
            buttons: true,
            dangerMode: true,
        }).then((willConfirm) => {
            if (willConfirm) {
                dispatch(keranjangAction.remove(index));
            }
        });
    }

    const checkoutHandler = () => {
        swal({
            title: "Checkout",
            text: "Apakah anda yakin melakukan pesanan?",
            icon: "info",
            buttons: true,
        }).then((willConfirm) => {
            if (willConfirm) {
                checkout();
            }
        });
    }

    const checkout = () => {

        let listObat = [];

        keranjangs.map(obat => {
            listObat.push(obat.id)
        })

        let data = {
            userId: id,
            listObat: listObat
        }

        const url = `buyers/checkout`;
        postData(url, data)
            .then(res => {
                swal("Sukses", res.data.message, "success");
                dispatch(keranjangAction.removeAll());
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            })

    }

    return (
        <>
            <Container className="mb-5">
                <Row className="judul-keranjang ">
                    <Col md="8">
                        <h4 className="mb-3">Keranjang Belanja</h4>
                        <hr />
                    </Col>
                    <Col md="4" className="checkout">
                        <div className="text-right">
                            <CurrencyFormat
                                value={subTotal}
                                displayType={"text"}
                                thousandSeparator={true}
                                prefix={"Rp. "}
                                renderText={(value) => (
                                    <p>
                                        Subtotal <br />
                                        <b><h5>{value}</h5></b>
                                    </p>
                                )}
                            />
                        </div>
                        <div>
                            {
                                totalKeranjang === 0 ?
                                    <button type="button" className="btn btn-checkout" disabled>
                                        <span className="bi bi-cart-check-fill"> Checkout</span>
                                    </button> :
                                    <button
                                        type="button"
                                        className="btn btn-checkout"
                                        onClick={() => checkoutHandler()}
                                    >
                                        <span className="bi bi-cart-check-fill"> Checkout</span>
                                    </button>
                            }
                        </div>
                    </Col>
                </Row>
                {
                    totalKeranjang === 0 ?
                        <div class="card card-kosong">
                            <div class="card-body card-kosong-item">
                                <div className="d-block text-center">
                                    <p>Keranjang anda masih kosong</p>
                                    <Link to="/buyer">
                                        <button
                                            type="button"
                                            className="btn btn-checkout"
                                        >
                                            <span className="bi bi-cart"> Belanja</span>
                                        </button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        :
                        <div className="content-keranjang">
                            {
                                keranjangs.map((data, index) =>
                                    <Row className="tabel-keranjang">
                                        <Col className="d-flex">
                                            <div className="p-2">
                                                <img
                                                    src={`data:image/*;base64, ` + data.dataImage}
                                                    className="image-obat-keranjang" alt="..."
                                                />
                                            </div>
                                            <div className="d-flex align-items-center">
                                                <div>
                                                    <b>
                                                        <h5>1 x {data.namaObat}</h5>
                                                    </b>
                                                    <h6>{data.jenisObat}</h6>
                                                </div>
                                            </div>
                                        </Col>
                                        <Col className="d-flex align-items-center justify-content-end">
                                            <div>
                                                <CurrencyFormat
                                                    value={data.hargaObat}
                                                    displayType={"text"}
                                                    thousandSeparator={true}
                                                    prefix={"Rp. "}
                                                    renderText={(value) => (
                                                        <p className="price">
                                                            {value}
                                                        </p>
                                                    )}
                                                />
                                            </div>
                                            <div>
                                                <span className="bi bi-x-circle" onClick={() => removeHandler(index)}></span>
                                            </div>
                                        </Col>
                                    </Row>
                                )
                            }
                        </div>
                }
            </Container>
        </>
    );
}

export default Keranjang;