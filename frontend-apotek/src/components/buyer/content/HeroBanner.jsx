import React from 'react'

const HeroBanner = () => {
    return (
        <div className="jumbotron jumbotron-fluid banner">
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h1 className="display-5">Solusi Kesehatan Terlengkap</h1>
                        <p className="lead">informasi seputar kesehatan, semua bisa di nexcares!</p>
                    </div>
                    <div className="col">

                    </div>
                </div>
            </div>
        </div>

    );
}

export default HeroBanner;