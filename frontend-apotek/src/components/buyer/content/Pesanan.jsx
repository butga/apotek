import React, { useEffect, useState } from 'react'
import CurrencyFormat from 'react-currency-format';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import swal from 'sweetalert';
import { getData } from '../../../service/Fetch';
import Footer from '../Footer';
import '../../../assets/css/pesanan.css'


const Pesanan = () => {

    const [orderan, setOrderan] = useState([]);

    const id = useSelector((state) => state.auth.user.id);

    useEffect(() => {
        const url = `buyers/orderans/${id}`;
        getData(url)
            .then(res => {
                setOrderan(res.data.data);
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            })
    }, [])

    return (
        <>
            <Container className="mb-5">
                <Row className="judul-keranjang ">
                    <Col md="12">
                        <h4 className="mb-4">Daftar Pesanan</h4>
                        <hr />
                    </Col>
                </Row>
                {
                    orderan.length === 0 ?
                        <div class="card card-kosong">
                            <div class="card-body card-kosong-item">
                                <div className="d-block text-center">
                                    <p>Pesanan anda masih kosong</p>
                                    <Link to="/buyer">
                                        <button
                                            type="button"
                                            className="btn btn-checkout"
                                        >
                                            <span className="bi bi-cart"> Belanja</span>
                                        </button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        :
                        <div className="konten-pesanan">
                            <table className="table text-center">
                                <thead className="thead-dark">
                                    <tr>
                                        <th scope="col">ID Order</th>
                                        <th scope="col">Tanggal Order</th>
                                        <th scope="col">Jumlah Obat</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        orderan.map( data =>
                                            <tr>
                                                <th scope="row">{data.id}</th>
                                                <td>{data.tanggalOrder}</td>
                                                <td>{data.jumlahOrder} Obat</td>
                                                <td>{data.status}</td>
                                            </tr>
                                        )
                                    }
                                </tbody>
                            </table>
                        </div>
                }
            </Container>
        </>
    );
}

export default Pesanan;