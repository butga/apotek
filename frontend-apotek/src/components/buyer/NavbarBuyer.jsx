import React, { useEffect, useState } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Container
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { authAction } from '../../redux/reducer/AuthReducer';
import { postData } from '../../service/Fetch';
import '../../assets/css/navbarAdmin.css'

const NavbarBuyer = () => {

    const [isOpen, setIsOpen] = useState(false);

    const dispatch = useDispatch();

    const toggle = () => setIsOpen(!isOpen);
    
    const id = useSelector((state) => state.auth.user.id);

    const total = useSelector((state) => state.keranjang.total);

    const logoutHandler = () => {
        dispatch(authAction.logout())
        postData(`users/logout/${id}`, {})
    }

    console.log(total);

    return (
        <div>
            <Navbar className="navbar-default" light expand="md">
                <Container>
                    <NavbarBrand>
                        <Link to="/buyer" className="navbar-items">
                            NEXCARES
                        </Link>
                    </NavbarBrand>
                    <NavbarToggler onClick={toggle} />
                    <Collapse isOpen={isOpen} navbar className="navbar-collapse-member">
                        <Nav navbar>
                            <div className="navbar navbar-items">
                                <Link to="/buyer/keranjang" className="navbar navbar-items">
                                    <span className="bi bi-cart"> <span class="badge badge-primary">{total}</span></span>
                                </Link>
                            </div>
                        </Nav>
                        <Nav navbar>
                            <div>
                                <UncontrolledDropdown className="navbar navbar-items float-right">
                                    <DropdownToggle nav caret className="navbar-items">
                                        <span className="bi bi-person-circle"> </span>
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem>
                                            <Link to="/buyer/account" className="navbar navbar-items">
                                                <span className="bi bi-person"> Akun</span>
                                            </Link>
                                        </DropdownItem>
                                        <DropdownItem>
                                            <Link to="/buyer/pesanan" className="navbar navbar-items">
                                                <span className="bi bi-journal-text"> Pesanan</span>
                                            </Link>
                                        </DropdownItem>
                                        <DropdownItem divider />
                                        <DropdownItem>
                                            <a
                                                href="#!"
                                                className="navbar navbar-items"
                                                onClick={logoutHandler}>
                                                <span className="bi bi-box-arrow-left"> Logout</span>
                                            </a>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </div>
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        </div>
    );
}

export default NavbarBuyer;