import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Col, Container, Row } from 'reactstrap';
import swal from 'sweetalert';
import '../../../assets/css/dashboardAdmin.css'
import { getData } from '../../../service/Fetch';
import LoadingComp from '../../LoadingComp';
import CardReport from '../card/CardReport';

const Dashboard = () => {

    const username = useSelector((state) => state.auth.user.username);

    const [data, setData] = useState()

    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const url = `admins/dashboard`;
        getData(url)
            .then(res => {
                setData(res.data.data);
                setIsLoading(false);
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            })
    }, [])

    console.log(data);

    return (
        <>
            {
                isLoading ?
                    <LoadingComp />
                    :
                    <>
                        <Container className="pt-3 mt-3">
                            <Row>
                                <Col>
                                    <h5 className="title">Welcome back, {username}!</h5>
                                </Col>
                            </Row>
                        </Container>
                        <Container className="mt-3">
                            <Row>
                                <Col sm="6" lg="4">
                                    <CardReport
                                        card="card-box bg-blue"
                                        amount={data.jumlahOrder}
                                        title="Pesanan hari ini."
                                        link="/admin/schedule"
                                        type="text"
                                    />
                                </Col>
                                <Col sm="6" lg="4">
                                    <CardReport
                                        card="card-box bg-green"
                                        amount={data.pendapatan}
                                        title="Pendapatan hari ini."
                                        link="/admin/schedule"
                                        type="number"
                                    />
                                </Col>
                            </Row>

                        </Container>
                    </>
            }
        </>
    );
}

export default Dashboard;