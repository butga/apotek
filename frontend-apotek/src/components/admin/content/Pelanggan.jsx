import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Col, Container, Row, Table, Button } from 'reactstrap';
import swal from 'sweetalert';
import { getData } from '../../../service/Fetch';
import BreadcrumbComp from '../../BreadcrumbComp';

const Pelanggan = () => {

    const user = useSelector((state) => state.auth.user);

    const [datas, setDatas] = useState([]);

    const [refresh, setRefresh] = useState();

    useEffect(() => {
        const url = `admins/users`;
        getData(url)
            .then(res => {
                setDatas(res.data.data);
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            })
    }, [refresh])

    console.log(datas);

    return (
        <Container className="pt-2">
            <BreadcrumbComp
                title="Data Pelanggan"
                link=""
                breadcrumb="Pelanggan"
            />

            <Row className="mt-4 mb-4">
                <Col>
                    {/* <FormAddRoom setResponse={setResponse} /> */}
                </Col>
            </Row>

            <Row className="mt-4 mb-4">
                <Col>
                    <Table striped bordered hover className="text-center">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>username</th>
                                <th>Email</th>
                                <th>Alamat</th>
                                <th>No Telp</th>
                                <th className="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                datas.length !== 0 ?
                                    datas.map((data) =>
                                        <tr key={data.id}>
                                            <td>{data.id}</td>
                                            <td>{data.namaLengkap}</td>
                                            <td>{data.namaLengkap}</td>
                                            <td>{data.email}</td>
                                            <td>{data.alamat}</td>
                                            <td>{data.no_telp}</td>
                                            <td>
                                                <div className="d-flex justify-content-center">

                                                    {
                                                        user.role === 'admin' ?
                                                            user.id === data.id ?
                                                                <td>
                                                                    <div className="d-flex justify-content-center">
                                                                        <Link to="account">
                                                                            <Button
                                                                                size="sm" color="warning"
                                                                                className="text-white btn-action"
                                                                            // onClick={toggle}
                                                                            >
                                                                                <i class="bi bi-pencil"></i>
                                                                            </Button>
                                                                        </Link>
                                                                        <Button
                                                                            size="sm" color="danger"
                                                                            className="text-white btn-action"
                                                                        // onClick={toggle}
                                                                        >
                                                                            <i class="bi bi-trash"></i>
                                                                        </Button>
                                                                    </div>
                                                                </td>
                                                                :
                                                                <td>
                                                                    <div className="d-flex justify-content-center">
                                                                        <Button
                                                                            size="sm" color="warning"
                                                                            className="text-white btn-action"
                                                                            // onClick={toggle}
                                                                        >
                                                                            <i class="bi bi-pencil"></i>
                                                                        </Button>
                                                                        <Button
                                                                            size="sm" color="danger"
                                                                            className="text-white btn-action"
                                                                            // onClick={toggle}
                                                                        >
                                                                            <i class="bi bi-trash"></i>
                                                                        </Button>
                                                                    </div>
                                                                </td>
                                                            :
                                                            <td>
                                                                <div className="d-flex justify-content-center">
                                                                    <Button
                                                                        size="sm" color="warning"
                                                                        className="text-white btn-action"
                                                                        // onClick={toggle}
                                                                        disabled
                                                                    >
                                                                        <i class="bi bi-pencil"></i>
                                                                    </Button>

                                                                    <Button
                                                                        size="sm" color="danger"
                                                                        className="text-white btn-action"
                                                                    // onClick={toggle}
                                                                    >
                                                                        <i class="bi bi-trash"></i>
                                                                    </Button>
                                                                </div>
                                                            </td>
                                                    }
                                                </div>
                                            </td>
                                        </tr>
                                    ) :
                                    <tr>
                                        <td colSpan="7" className="text-center">Data Kosong</td>
                                    </tr>
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
    );
}

export default Pelanggan;