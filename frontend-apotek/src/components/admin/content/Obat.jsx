import React, { useEffect, useState } from 'react';
import CurrencyFormat from 'react-currency-format';
import '../../../assets/css/obat.css'
import BreadcrumbComp from '../../BreadcrumbComp';
import { getData, postData } from '../../../service/Fetch';
import TambahObat from '../form/TambahObat';
import EditObat from '../form/EditObat';
import swal from 'sweetalert';
import {
    Col,
    Container,
    Row,
    Table,
    Button
} from 'reactstrap';


const Obat = () => {

    const [datas, setDatas] = useState([]);

    const [refresh, setRefresh] = useState();

    useEffect(() => {
        const url = `admin/obats`;
        getData(url)
            .then(res => {
                setDatas(res.data.data);
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            })
    }, [refresh])

    const deleteHandler = (id) => {
        swal({
            title: "Hapus data obat?",
            text: "Apakah anda yakin untuk menghapus data ini?",
            icon: "info",
            buttons: true,
        }).then((willConfirm) => {
            if (willConfirm) {
                softDelete(id)
            }
        });
    }

    const softDelete = (id) => {
        postData(`admin/obats/delete/${id}`)
            .then(res => {
                setRefresh(res.data.data);
                swal("Sukses", res.data.message, "success");
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            })
    }

    return (
        <Container className="pt-2">
            <BreadcrumbComp
                title="Data Obat"
                link="admin"
                breadcrumb="obat"
            />
            <Row className="mt-4 mb-4">
                <Col>
                    <TambahObat
                        setRefresh={setRefresh}
                    />
                </Col>
            </Row>
            <Row className="mt-4 mb-4">
                <Col>
                    <Table
                        striped
                        bordered
                        hover
                        className="text-center align-middle"
                    >
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Gambar</th>
                                <th>Nama</th>
                                <th>Jenis</th>
                                <th>Stok</th>
                                <th>Harga</th>
                                <th>Dosis</th>
                                <th className="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                datas.length !== 0 ?
                                    datas.map((obat) =>
                                        <tr key={obat.id}>
                                            <td>{obat.id}</td>
                                            <td>
                                                <img
                                                    src={`data:image/*;base64, ` + obat.dataImage}
                                                    className="image-obat-tabel" alt="..."
                                                />
                                            </td>
                                            <td>{obat.namaObat}</td>
                                            <td>{obat.jenisObat}</td>
                                            <td>{obat.stokObat}</td>
                                            <CurrencyFormat
                                                value={obat.hargaObat}
                                                displayType={"text"}
                                                thousandSeparator={true}
                                                prefix={"Rp. "}
                                                renderText={(value) => (
                                                    <td>{value}</td>
                                                )}
                                            />
                                            <td>{obat.dosis}</td>
                                            <td>
                                                <div className="d-flex justify-content-center">
                                                    <EditObat
                                                        obat={obat}
                                                        setRefresh={setRefresh}
                                                    />
                                                    <Button
                                                        size="sm" color="danger"
                                                        className="text-white btn-action"
                                                        onClick={() => deleteHandler(obat.id)}
                                                    >
                                                        <i class="bi bi-trash"></i>
                                                    </Button>
                                                </div>
                                            </td>
                                        </tr>
                                    ) :
                                    <tr>
                                        <td colSpan="8" className="text-center">Data Kosong</td>
                                    </tr>
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
    );
}

export default Obat;