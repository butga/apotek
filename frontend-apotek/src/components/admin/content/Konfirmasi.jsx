import { useEffect, useState } from "react";
import swal from "sweetalert";
import { getData, postData } from "../../../service/Fetch";
import BreadcrumbComp from "../../BreadcrumbComp";
import { Button } from 'reactstrap';
import '../../../assets/css/konfirmasi.css'

const Konfirmasi = () => {

    const [orderan, setOrderan] = useState([]);

    const [response, setResponse] = useState();

    useEffect(() => {
        const url = `buyers/orderans/confirm`;
        getData(url)
            .then(res => {
                setOrderan(res.data.data);
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            })
    }, [response])

    const approveHandler = (id) => {
        swal({
            title: "Setujui Pesanan?",
            text: "Apakah anda yakin untuk menyetujui pesanan ini?",
            icon: "info",
            buttons: true,
        })
            .then((willConfirm) => {
                if (willConfirm) {
                    postData(`admin/orderans/confirm/${id}`, {})
                        .then(res => {
                            setResponse(res.data.data)
                            swal({
                                title: "Sukses!",
                                text: "Berhasil Konfirmasi Pesanan",
                                icon: "success",
                            })
                        })
                        .catch(err => {
                            swal("Error", err.response.data, "error");
                        });
                }
            });
    }

    const rejectHandler = (id) => {
        swal({
            title: "Tolak Pesanan?",
            text: "Apakah anda yakin menolak pesanan ini?",
            icon: "info",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    postData(`admin/orderans/reject/${id}`, {})
                        .then(res => {
                            setResponse(res.data)
                            swal({
                                title: "Success!",
                                text: "Success reject this order",
                                icon: "success",
                            });
                        })
                        .catch(err => {
                            swal("Error", err.response.data, "error");
                        });
                }
            });
    }

    return (
        <div className="container">
            <BreadcrumbComp
                title="Pesanan"
                link="admin"
                breadcrumb="Konfirmasi Pesanan"
            />
            <div className="row">
                <div className="col">
                    <table className="table text-center">
                        <thead className="thead-dark">
                            <tr>
                                <th scope="col">ID Order</th>
                                <th scope="col">Tanggal Order</th>
                                <th scope="col">Jumlah Obat</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                orderan.length !== 0 ?
                                    orderan.map(data =>
                                        <tr>
                                            <th scope="row">{data.id}</th>
                                            <td>{data.tanggalOrder}</td>
                                            <td>{data.jumlahOrder} Obat</td>
                                            <td>{data.status}</td>
                                            <td>
                                                <Button
                                                    onClick={() => approveHandler(data.id)}
                                                    className="btn-approve">
                                                    <i class="bi bi-check2-square"></i>
                                                </Button>
                                                <Button
                                                    onClick={() => rejectHandler(data.id)}
                                                    className="btn-reject">
                                                    <i class="bi bi-x-square"></i>
                                                </Button>
                                            </td>
                                        </tr>
                                    ) :
                                    <tr>
                                        <td colSpan="5">Data Kosong</td>
                                    </tr>
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default Konfirmasi;