import { useEffect, useState } from "react";
import swal from "sweetalert";
import { getData } from "../../../service/Fetch";
import BreadcrumbComp from "../../BreadcrumbComp";

const Laporan = () => {

    const [orderan, setOrderan] = useState([]);

    useEffect(() => {
        const url = `buyers/orderans`;
        getData(url)
            .then(res => {
                setOrderan(res.data.data);
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            })
    }, [])

    console.log(orderan);

    return (
        <div className="container">
            <BreadcrumbComp
                title="Pesanan"
                link="admin"
                breadcrumb="Laporan Pesanan"
            />
            <div className="row">
                <div className="col">
                    <table className="table text-center">
                        <thead className="thead-dark">
                            <tr>
                                <th scope="col">ID Order</th>
                                <th scope="col">Tanggal Order</th>
                                <th scope="col">Jumlah Obat</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                orderan.length !== 0 ?
                                    orderan.map(data =>
                                        <tr>
                                            <th scope="row">{data.id}</th>
                                            <td>{data.tanggalOrder}</td>
                                            <td>{data.jumlahOrder} Obat</td>
                                            <td>{data.status}</td>
                                        </tr>
                                    ) :
                                    <tr>
                                        <td colSpan="4">Data Kosong</td>
                                    </tr>
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default Laporan;