import React, { useState } from 'react';
import swal from 'sweetalert';
import { postData } from '../../../service/Fetch';
import '../../../assets/css/custom.css';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Form,
    FormGroup,
    Label,
    Input,
    Row
} from 'reactstrap';


const TambahObat = (props) => {

    const [modal, setModal] = useState(false);

    const toggle = () => {
        setModal(!modal);
    }

    const [datas, setDatas] = useState({
        namaObat: '',
        jenisObat: '',
        stokObat: '',
        hargaObat: '',
        dosis: '',
        dataImage: ''
    });

    const submitHandler = (event) => {
        event.preventDefault();
        postData(`admin/obats`, datas)
            .then(res => {
                props.setRefresh(res.data.data)
                swal("Sukses", res.data.message, "success")
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            });
        toggle();
    }

    const imageHandler = (e) => {
        e.preventDefault();
        let file = e.target.files[0];
        let reader = new FileReader();
        reader.onloadend = () => {
            convertBase64(reader.result)
        }
        reader.readAsDataURL(file)
    }

    const convertBase64 = (imagePreview) => {
        let data = imagePreview.split(',')[1];
        let raw = window.atob(data);
        let rawLength = raw.length;

        let array = new Uint8Array(new ArrayBuffer(rawLength));
        for (let i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }

        let imageBlob = [];
        for (let i = 0; i < array.length; i++) {
            imageBlob.push(array[i]);
        }

        setDatas({ ...datas, dataImage: imageBlob })
    }

    console.log(datas);

    return (
        <div>
            <Button
                size="sm"
                color="primary"
                className="text-white btn-action"
                onClick={toggle}>
                <span className="bi bi-plus-square"> Tambah Obat</span>
            </Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Tambah Obat</ModalHeader>
                <ModalBody>
                    <Form onSubmit={submitHandler}>
                        <FormGroup>
                            <Label for="name" className="mb-2">Nama Obat</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                onChange={(e) => setDatas({ ...datas, namaObat: e.target.value })}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="name" className="mb-2">Jenis Obat</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                onChange={(e) => setDatas({ ...datas, jenisObat: e.target.value })}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="name" className="mb-2">Stok</Label>
                            <Input type="number" min="1" name="name" id="name" className="mb-2"
                                onChange={(e) => setDatas({ ...datas, stokObat: e.target.value })}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="name" className="mb-2">Dosis</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                onChange={(e) => setDatas({ ...datas, dosis: e.target.value })}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="name" className="mb-2">Harga</Label>
                            <Input type="number" min="1" name="name" id="name" className="mb-2"
                                onChange={(e) => setDatas({ ...datas, hargaObat: e.target.value })}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="image" className="mb-2">Image</Label>
                            <Row>
                                <Input type="file" name="image" id="image"
                                    onChange={(e) => imageHandler(e)}
                                    required />
                            </Row>
                        </FormGroup>

                        <div className="d-flex justify-content-end">
                            <input type="submit" className="btn btn-primary m-2"
                                value="submit" />
                            <input type="button" className="btn btn-secondary m-2"
                                value="Cancel" onClick={toggle} />
                        </div>
                    </Form>
                </ModalBody>
            </Modal>
        </div>
    );
}

export default TambahObat;