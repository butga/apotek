import React, { useState } from 'react';
import swal from 'sweetalert';
import { 
    Button, 
    Modal, 
    ModalHeader, 
    ModalBody, 
    Form, 
    FormGroup, 
    Label, 
    Input 
} from 'reactstrap';
import { postData } from '../../../service/Fetch';
import '../../../assets/css/custom.css';


const EditObat = (props) => {

    const [modal, setModal] = useState(false);

    const toggle = () => {
        setModal(!modal);
    }

    const [datas, setDatas] = useState({
        namaObat: props.obat.namaObat,
        jenisObat: props.obat.jenisObat,
        stokObat: props.obat.stokObat,
        hargaObat: props.obat.hargaObat,
        dosis: props.obat.dosis,
    });

    const submitHandler = (event) => {
        event.preventDefault();

        postData(`admin/obats/${props.obat.id}`, datas)
            .then(res => {
                props.setRefresh(res.data.data)
                swal("Sukses", res.data.message, "success")
            })
            .catch(err => {
                swal("Error", err.response.data.message, "error");
            });

        toggle();
    }

    return (
        <div>
            <Button size="sm" color="warning" className="text-white btn-action" onClick={toggle}><i class="bi bi-pencil"></i></Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Edit Obat</ModalHeader>
                <ModalBody>
                    <Form onSubmit={submitHandler}>
                        <FormGroup>
                            <Label for="name" className="mb-2">Nama Obat</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                onChange={(e) => setDatas({ ...datas, namaObat: e.target.value })}
                                value={datas.namaObat}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="name" className="mb-2">Jenis Obat</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                onChange={(e) => setDatas({ ...datas, jenisObat: e.target.value })}
                                value={datas.jenisObat}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="name" className="mb-2">Stok</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                onChange={(e) => setDatas({ ...datas, stokObat: e.target.value })}
                                value={datas.stokObat}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="name" className="mb-2">Dosis</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                onChange={(e) => setDatas({ ...datas, dosis: e.target.value })}
                                value={datas.dosis}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <Label for="name" className="mb-2">Harga</Label>
                            <Input type="text" name="name" id="name" className="mb-2"
                                onChange={(e) => setDatas({ ...datas, hargaObat: e.target.value })}
                                value={datas.hargaObat}
                                required />
                        </FormGroup>
                        <div className="d-flex justify-content-end">
                            <input type="submit" className="btn btn-primary m-2" 
                                value="submit" />
                            <input type="button" className="btn btn-secondary m-2" 
                                value="Cancel" onClick={toggle} />
                        </div> 
                    </Form>
                </ModalBody>
            </Modal>
        </div>
    );
}

export default EditObat;