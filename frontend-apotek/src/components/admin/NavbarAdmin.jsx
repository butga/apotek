import React, { useState } from 'react';
import '../../assets/css/navbarAdmin.css'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Container
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { authAction } from '../../redux/reducer/AuthReducer';
import { useSelector } from 'react-redux';
import { postData } from '../../service/Fetch';

const NavbarAdmin = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    const dispatch = useDispatch();

    const id = useSelector((state) => state.auth.user.id);

    const logoutHandler = () => {
        dispatch(authAction.logout())
        postData(`users/logout/${id}`, {})
    }

    return (
        <div>
            <Navbar className="navbar-default" light expand="md">
                <Container>
                    <NavbarBrand>
                        NEXCARES
                    </NavbarBrand>
                    <NavbarToggler onClick={toggle} />
                    <Collapse isOpen={isOpen} navbar className="navbar-collapse-admin">
                        <Nav navbar>
                            <Link to="/admin" className="navbar navbar-items">
                                Dashboard
                            </Link>
                            <Link to="/admin/obat" className="navbar navbar-items">
                                Obat
                            </Link>
                            <Link to="/admin/user" className="navbar navbar-items">
                                Pelanggan
                            </Link>
                            <UncontrolledDropdown className="navbar navbar-items">
                                <DropdownToggle nav caret>
                                    Pemesanan
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem>
                                        <Link to="/admin/order-request" className="navbar navbar-items">
                                            <span className="bi bi-journal-text"> Konfirmasi Resep</span>
                                        </Link>
                                    </DropdownItem>
                                    <DropdownItem>
                                        <Link to="/admin/konfirmasi" className="navbar navbar-items">
                                            <span className="bi bi-journal-check"> Konfirmasi Pesanan</span>
                                        </Link>
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                            <Link to="/admin/laporan" className="navbar navbar-items">
                                Laporan
                            </Link>
                        </Nav>
                        <Nav navbar>
                            <UncontrolledDropdown className="navbar navbar-items">
                                <DropdownToggle nav caret>
                                    <span className="bi bi-person-circle"> </span>
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem>
                                        <Link to="/admin/account" className="navbar navbar-items">
                                            <span className="bi bi-person"> Account</span>
                                        </Link>
                                    </DropdownItem>
                                    <DropdownItem>
                                        <a
                                            href="#!"
                                            className="navbar navbar-items"
                                            onClick={logoutHandler}>
                                            <span className="bi bi-box-arrow-left"> Logout</span>
                                        </a>
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        </div>
    );
}

export default NavbarAdmin;